// 2. Total number of item supplied by Yellow Farms and has a price less than 50.

db.fruits.aggregate([

    {$match: {$and:[{price:{$gt:50}},{supplier:"Yellow Farms"}]}},
    {$count:"YellowFarmBelow50"}

])

// 3. Aggregate to count the total number of items with price less than 30.

db.fruits.aggregate([

    {$match: {price:{$lt:30}}},
    {$count:"priceLesserThan30"}

])

// 4. Aggregate to get the average price of fruits supplied by yellow farm

db.fruits.aggregate([

    {$match:{supplier:"Yellow Farms"}},
    {$group:{_id:"$supplier", avgPrice:{$avg: "$price"}}}

])

// 5. Aggregate to get the highest price of fruits supplied by Red Farms Inc.

db.fruits.aggregate([

    {$match: {supplier: "Red Farms Inc."}},
    {$group: {_id: "$supplier", maxPrice: {$max: "$price"}}}

])

// 6. Aggregate to get the lowest price of fruits supplied by Red Farms Inc.

db.fruits.aggregate([

    {$match: {supplier: "Red Farms Inc."}},
    {$group: {_id: "$supplier", minPrice: {$min: "$price"}}}

])